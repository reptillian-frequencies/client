FROM node:12.7-alpine
WORKDIR /src/app
COPY package.json ./
RUN npm install
COPY . .

RUN npm install -g @angular/cli@7.3.9
RUN ng build --prod
EXPOSE 8082
CMD ng serve --host 0.0.0.0 --disableHostCheck true --port 8082

