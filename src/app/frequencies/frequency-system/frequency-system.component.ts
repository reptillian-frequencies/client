import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import * as FileSaver from 'file-saver';
import { Subscription } from 'rxjs';

import { FrequencyResponse } from '../../entities/frequency-response';
import { Frequency } from '../../entities/frequency';

import { FrequenciesService } from '../../services/frequencies.service';
import { AlertService } from '../../services/alert.service';
import { FrequencySubscriptionService } from '../../services/frequency-subscription.service';

@Component({
  selector: 'app-frequency-system',
  templateUrl: './frequency-system.component.html',
  styleUrls: ['./frequency-system.component.css']
})
export class FrequencySystemComponent implements OnInit, OnDestroy {
    generate = 'Generate';
    download = 'Download';
    generateFrequencyResponseSubscription: Subscription;
    downloadFrequencyResponseSubscription: Subscription;

    constructor(
    private frequenciesService: FrequenciesService,
    private alertService: AlertService,
    private frequencySubscriptionService: FrequencySubscriptionService
    ) {    }

    ngOnInit() {
    this.generateFrequencyResponseSubscription = this.frequencySubscriptionService.generateFrequency$
           .subscribe(frequency => this.frequencyGenerateRequest(frequency));
    this.downloadFrequencyResponseSubscription = this.frequencySubscriptionService.downloadFrequency$
           .subscribe(frequency => this.frequencyDownloadRequest(frequency));
    }

    @HostListener('window:beforeunload')
    ngOnDestroy() {
      if (this.generateFrequencyResponseSubscription) {
        this.generateFrequencyResponseSubscription.unsubscribe();
      }
      if (this.downloadFrequencyResponseSubscription) {
        this.downloadFrequencyResponseSubscription.unsubscribe();
      }
    }

    emitGenerateFrequency(frequency:Frequency) {
      this.frequencySubscriptionService.nextGenerateFrequencySubject(frequency);
    }

    emitDownloadFrequency(frequency:Frequency) {
      this.frequencySubscriptionService.nextDownloadFrequencySubject(frequency);
    }

    frequencyGenerateRequest(frequency:Frequency) {
      if(frequency == null) {
        return;
      }
      this.frequenciesService.generateFrequency(frequency)
      .subscribe((frequencyResponse:FrequencyResponse) => this.handleGenerateResponse(frequencyResponse, frequency));
    }

    frequencyDownloadRequest(frequency:Frequency) {
      if(frequency == null) {
        return;
      }
      this.frequenciesService.downloadFrequency(frequency)
      .subscribe((frequencyResponse:FrequencyResponse) => this.handleDownloadResponse(frequencyResponse, frequency));
    }

    handleGenerateResponse(frequencyResponse:FrequencyResponse, frequency:Frequency) {
      if(!frequencyResponse.succeeded) {
        this.alertService.showErrorAlert(`Generating frequency '${frequency.name}' in size
         ${frequency.size}B has failed:  ${frequencyResponse.errorMessage}`);
        return;
      }
      this.alertService.showSuccessAlert(`Generated frequency '${frequency.name}' in size
       ${frequency.size}B Successfully!`);
    }

    handleDownloadResponse(frequencyResponse:FrequencyResponse, frequency:Frequency) {
      if(!frequencyResponse.succeeded) {
        this.alertService.showErrorAlert(`Downloading frequency '${frequency.name}' in size
         ${frequency.size}B has failed: ${frequencyResponse.errorMessage}`);
        return;
      }
      this.downloadFrequencyFromResponse(frequency, frequencyResponse);
    }

    downloadFrequencyFromResponse(frequency:Frequency, frequencyResponse:FrequencyResponse) {
      const filename = `frequency-${frequency.name}-${frequency.size}.bin`;
      this.alertService.showInfoAlert(`Downloading frequency '${frequency.name}' in size
       ${frequency.size}B...`);
      this.downloadData(atob(frequencyResponse.frequencyContent), filename);
    }

    downloadData(data, filename:string) {
      const blob = new Blob([data], { type: 'application/octet-stream' });
      FileSaver.saveAs(blob, filename);
    }
}
