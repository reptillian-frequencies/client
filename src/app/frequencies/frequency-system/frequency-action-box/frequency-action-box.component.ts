import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { Frequency } from '../../../entities/frequency';

import { AlertService } from '../../../services/alert.service'
import { FrequencySubscriptionService } from '../../../services/frequency-subscription.service';

@Component({
  selector: 'app-frequency-action-box',
  templateUrl: './frequency-action-box.component.html',
  styleUrls: ['./frequency-action-box.component.css']
})
export class FrequencyActionBoxComponent {

  readonly NUMBERS_LETTERS_OR_UNDERSCORE_REGEX = /^[A-Za-z0-9_]+$/;
  readonly DEFAULT_NAME = '';
  readonly DEFAULT_SIZE = 32;
  readonly EMPTY_SIZE = 0;
  frequenciesForm;
  frequency: Frequency = {
    name: this.DEFAULT_NAME,
    size: this.EMPTY_SIZE
    };
  @Input() action: string;
  @Output() frequencyRequestEvent = new EventEmitter();

  constructor(
      private formBuilder: FormBuilder,
      private alertService: AlertService,
      private frequencySubscriptionService: FrequencySubscriptionService
      ) {
        this.frequenciesForm = this.formBuilder.group({
            name: this.DEFAULT_NAME,
            size: this.DEFAULT_SIZE
            });
      }


  onSubmit(frequencyData) {
     if(this.isInvalid(frequencyData.name)) {
      this.alertService.showErrorAlert("Entered invalid name.");
      return;
     }
     this.frequency.name = frequencyData.name;
     this.frequency.size = frequencyData.size;

     this.frequencyRequestEvent.emit(this.frequency);
    }

  isInvalid(name) {
    return(!this.NUMBERS_LETTERS_OR_UNDERSCORE_REGEX.test(name));
  }
}
