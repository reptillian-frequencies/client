export interface FrequencyResponse {
  succeeded: boolean;
  errorMessage: string;
  frequencyContent: string;
}

