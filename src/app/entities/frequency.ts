export interface Frequency {
  name: string;
  size: number;
}
