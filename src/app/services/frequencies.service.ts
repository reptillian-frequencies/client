import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Frequency } from '../entities/frequency';
import { FrequencyResponse } from '../entities/frequency-response';
import { AlertService } from './alert.service'


@Injectable({
  providedIn: 'root'
})
export class FrequenciesService {

  readonly SERVER_URL = environment.serverUrl;

    constructor(
      private http: HttpClient,
      private alertService: AlertService
    ) { }

    generateFrequency(frequency : Frequency): Observable<FrequencyResponse> {
      const generateURL = `${this.SERVER_URL}/frequencies/generate/${frequency.size}/${frequency.name}`;
      return this.http.post<FrequencyResponse>(generateURL, frequency).pipe(
        catchError(error => this.handleError(error))
        );
    }

    downloadFrequency(frequency : Frequency): Observable<FrequencyResponse> {
      const downloadURL = `${this.SERVER_URL}/frequencies/download/${frequency.size}/${frequency.name}`;
      const options = {
        responseType: 'json' as const,
        observe: 'body' as const,
      };
      return this.http.get<FrequencyResponse>(downloadURL, options).pipe(
        catchError(error => this.handleError(error))
        );
     }

    handleError(error) {
      this.alertService.showErrorAlert("Could not connect to server. Try again later.");
      return throwError(error);
    }
}
