import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs';

import { Frequency } from '../entities/frequency';

@Injectable({
  providedIn: 'root',
})
export class FrequencySubscriptionService {

  private generateFrequencySubject = new BehaviorSubject<Frequency>(null);
  private downloadFrequencySubject = new BehaviorSubject<Frequency>(null);

  generateFrequency$ = this.generateFrequencySubject.asObservable();
  downloadFrequency$ = this.downloadFrequencySubject.asObservable();

  nextGenerateFrequencySubject(frequency:Frequency) {
    this.generateFrequencySubject.next(frequency);
  }

  nextDownloadFrequencySubject(frequency:Frequency) {
    this.downloadFrequencySubject.next(frequency);
  }
}
