import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root',
})

export class AlertService {

  constructor(private toastr:ToastrService) {}

   showSuccessAlert(message:string) {
     this.toastr.success(message, "SUCCESS!");
   }

   showErrorAlert(message:string) {
     this.toastr.error(message, "ERROR!");
   }

   showInfoAlert(message:string) {
     this.toastr.info(message,'', {
     progressBar: false
     });
   }

}
