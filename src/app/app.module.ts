import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { FrequencySystemComponent } from './frequencies/frequency-system/frequency-system.component';
import { FrequencyActionBoxComponent } from './frequencies/frequency-system/frequency-action-box/frequency-action-box.component';

@NgModule({
  declarations: [
    AppComponent,
    FrequencySystemComponent,
    FrequencyActionBoxComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
    timeOut:5000,
    closeButton: true,
    positionClass: 'toast-top-right',
    progressBar: true
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
